package ua.danit.controller;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import ua.danit.model.Album;
import ua.danit.model.Track;
import ua.danit.service.AlbumService;
import ua.danit.service.TrackService;

/**
 * Created  26.03.2018.
 */

@RestController
@RequestMapping("/")
public class AlbumControler {

  @Autowired
  private AlbumService albumService;
  @Autowired
  private TrackService trackService;


  @RequestMapping(path = "/albums", method = RequestMethod.POST)
  public void addAlbum(@RequestBody Album album) {
    System.out.println(album);
    albumService.addAlbum(album);
    System.out.println(album.toString());
  }

  @RequestMapping(value = "/albums", method = RequestMethod.GET)
  @ResponseBody
  @CrossOrigin(value = "http://localhost:3000")
  Iterable<Album> getAllAlbum() {
    return albumService.getAllAlbum();
  }

  @RequestMapping(path = "/albums/", method = RequestMethod.GET)
  @ResponseBody
  @CrossOrigin(value = "http://localhost:3000")
  Album getAlbumById(int id) {
    return albumService.getAlbumById(id);
  }

  @RequestMapping(path = "/albums/{id}", method = RequestMethod.PUT)
  void update(@PathVariable int id, @RequestBody Album album) {
    albumService.update(id, album);
  }

  @RequestMapping(path = "/albums/{id}", method = RequestMethod.DELETE)
  void delete(@PathVariable int id) {
    albumService.delete(id);
  }

  @RequestMapping(path = "/albums/{id}/cover", method = RequestMethod.DELETE)
  void deleteCover(@PathVariable int id) {
    albumService.deleteCover(id);
  }
//создаем альбом с песнями
  @RequestMapping(path = "/create")
  Album getAlbum() {
    Album album = new Album();
    album.setTitle("Sting : Ten Summoner's Tales");
    album.setCover("https://upload.wikimedia.org/wikipedia/en/thumb/9/9f/Ten_Summoner%27s_Tales.jpg/220px-Ten_Summoner%27s_Tales.jpg");

    Track track1 = new Track();
    track1.setTrackName("Shape of My Heart");
    track1.setTrackUpload("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3");

    Track track2 = new Track();
    track2.setTrackName("If I Ever Lose My Faith In You");
    track2.setTrackUpload("https://www.soundhelix.com/examples/mp3/SoundHelix-Song-2.mp3");
    album.setTracks(Arrays.asList(track1,track2));

    return albumService.addAlbum(album);
  }


}
